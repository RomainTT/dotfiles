" Vim configuration for python files
" ----------------------------------

" Indentation
setlocal tabstop=4 
setlocal softtabstop=4 
setlocal shiftwidth=4 
setlocal textwidth=87
setlocal expandtab 
setlocal autoindent 
setlocal fileformat=unix

" UTF-8 encoding by default
setlocal encoding=utf-8

" Flag unnecessary whitespaces
highlight BadWhitespace ctermbg=red guibg=darkred
call matchadd('BadWhitespace', '\s\+$')

" Highlight text that exceeds 88 characters
highlight OverLength ctermbg=darkred ctermfg=white guibg=darkred
call matchadd('OverLength', '\%>88v.\+')

