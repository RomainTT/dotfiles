" Indentation
setlocal tabstop=4 
setlocal softtabstop=4 
setlocal shiftwidth=4 
setlocal textwidth=87
setlocal expandtab 
setlocal autoindent 
setlocal fileformat=unix

" UTF-8 encoding by default
setlocal encoding=utf-8
