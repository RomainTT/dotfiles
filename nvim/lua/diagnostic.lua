-- Enable diagnostics
vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(
		vim.lsp.diagnostic.on_publish_diagnostics, {
				virtual_text = true,
				signs = true,
				update_in_insert = true,
				focusable = false,
		}
)

-- Set updatetime for CursorHold
-- 300ms of no cursor movement to trigger CursorHold
vim.opt.updatetime = 300
-- Show diagnostic popup on cursor hold
vim.api.nvim_create_autocmd(
		{'CursorHold'},
		{ pattern = { '*' }, callback = vim.diagnostic.open_float }
)

-- Goto previous/next diagnostic warning/error
vim.keymap.set('n', '<leader>lp', vim.diagnostic.goto_prev, { silent = true })
vim.keymap.set('n', '<leader>ln', vim.diagnostic.goto_next, { silent = true })
