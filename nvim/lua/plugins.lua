-- This file declares all of the plugins
------------------------------------------------------------------------------

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
		-- Packer can manage itself
		use 'wbthomason/packer.nvim'

		-- Common plugins
		use 'nvim-lua/plenary.nvim' -- Lua functions used by other plugins

		-- Plugins for telescope
		use 'nvim-telescope/telescope-fzy-native.nvim'
		use 'nvim-telescope/telescope.nvim'
		use 'nvim-tree/nvim-web-devicons'
		use 'nvim-treesitter/nvim-treesitter'

		-- Plugins about interface
		use 'scrooloose/nerdtree'
		use 'vim-airline/vim-airline'
		use 'vim-airline/vim-airline-themes'
		use 'kana/vim-submode'
		use 'gcmt/taboo.vim'
		use 'preservim/tagbar'
		use 'morhetz/gruvbox'

		-- Plugins about python
		use 'Shougo/echodoc.vim'
		use 'neomake/neomake'
		use {'linux-cultist/venv-selector.nvim', branch='regexp'}

		-- Plugins about git
		use 'airblade/vim-gitgutter'
		use 'Xuyuanp/nerdtree-git-plugin'
		use 'tpope/vim-fugitive'
		use 'christoomey/vim-conflicted'
		use 'iberianpig/tig-explorer.vim'

		-- Plugins for autocompletion and syntax checking
		use 'neovim/nvim-lspconfig' -- Collection of configurations for built-in LSP client
		use 'williamboman/mason.nvim' -- Makes installation of external tools easier (lsp, linters, etc)
		use 'williamboman/mason-lspconfig.nvim' -- Bridges Mason with lspconfig
		use 'hrsh7th/nvim-cmp' -- Completion engine
		use 'hrsh7th/cmp-buffer' -- To complete buffers
		use 'hrsh7th/cmp-path' -- To complete filesystem paths
		use 'hrsh7th/cmp-cmdline' -- To complete command lines
		use 'hrsh7th/cmp-nvim-lsp' -- To complete entries from LSP
		use 'hrsh7th/cmp-nvim-lsp-signature-help' -- To complete function signatures
		use 'hrsh7th/cmp-emoji' -- To complete markdown emojis
		use 'f3fora/cmp-spell'

		-- Plugins for snippets
		use 'hrsh7th/cmp-vsnip'
		use 'hrsh7th/vim-vsnip'
		use 'rafamadriz/friendly-snippets'

		--use 'folke/trouble.nvim' " pretty list for diagnostics, references, an all.
		use 'mhartington/formatter.nvim'

		-- Other plugins to make edition easier
		use 'pseewald/vim-anyfold'
		use 'scrooloose/nerdcommenter'
		use 'kshenoy/vim-signature'
		use 'Yggdroot/indentLine'
		use 'tpope/vim-surround'
		use({
			"andrewferrier/wrapping.nvim",
			config = function()
				require("wrapping").setup()
			end,
		})

		-- A use to use VIM for todo lists
		use 'freitass/todo.txt-vim'

		-- A use for CSV files
		use 'mechatroner/rainbow_csv'

		-- Use VIM for texinfo documentation
		use 'HiPhish/info.vim'
end)
