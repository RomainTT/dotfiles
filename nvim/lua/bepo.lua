-- If this file is used as an additional configuration aside from another one
-- with Vundle plugins for instance, then I recommend to source .vimrc.bepo
-- at first.
-- --------------------------------------------------------------------------------

-- Change map leader
vim.g.mapleader = "è"
vim.g.maplocalleader = "è"

-- {W} -> [É]
-- ——————————
-- On remappe W sur É :
vim.keymap.set('', 'é', 'w')
vim.keymap.set('', 'É', 'W')
-- Corollaire: on remplace les text objects aw, aW, iw et iW
-- pour effacer/remplacer un mot quand on n’est pas au début (daé / laé).
vim.keymap.set('o', 'aé', 'aw')
vim.keymap.set('o', 'aÉ', 'aW')
vim.keymap.set('o', 'ié', 'iw')
vim.keymap.set('o', 'iÉ', 'iW')

-- [HJKL] -> {CTSR}
-- ————————————————
-- {cr} = « gauche / droite »
vim.keymap.set('', 'c', 'h')
vim.keymap.set('', 'r', 'l')
-- {ts} = « haut / bas »
vim.keymap.set('', 't', 'j')
vim.keymap.set('', 's', 'k')
-- {CR} = « haut / bas de l'écran »
vim.keymap.set('', 'C', 'H')
vim.keymap.set('', 'R', 'L')
-- {TS} = « joindre / aide »
vim.keymap.set('n', 'T', 'J')
vim.keymap.set('n', 'S', 'K')
-- Corollaire : repli suivant / précédent
vim.keymap.set('n', 'zs', 'zj')
vim.keymap.set('n', 'zt', 'zk')

-- {HJKL} <- [CTSR]
-- ————————————————
-- {J} = « Jusqu'à »            (j = suivant, J = précédant)
vim.keymap.set('', 'j', 't')
vim.keymap.set('', 'J', 'T')
-- {L} = « Change »             (l = attend un mvt, L = jusqu'à la fin de ligne)
vim.keymap.set('', 'l', 'c')
vim.keymap.set('', 'L', 'C')
-- {H} = « Remplace »           (h = un caractère slt, H = reste en « Remplace »)
vim.keymap.set('', 'h', 'r')
vim.keymap.set('', 'H', 'R')
-- {K} = « Substitue »          (k = caractère, K = ligne)
vim.keymap.set('', 'k', 's')
vim.keymap.set('', 'K', 'S')
-- Corollaire : correction orthographique
vim.keymap.set('', ']k', ']s')
vim.keymap.set('', '[k', '[s')

-- Désambiguation de {g}
-- —————————————————————
-- ligne écran précédente / suivante (à l'intérieur d'une phrase)
vim.keymap.set('n', 'gs', 'gk')
vim.keymap.set('n', 'gt', 'gj')
-- onglet précédant / suivant
vim.keymap.set('n', 'gb', 'gT')
vim.keymap.set('n', 'gé', 'gt')
-- optionnel : {gB} / {gÉ} pour aller au premier / dernier onglet
vim.keymap.set('n', 'gB', ':exe "silent! tabfirst"<CR>')
vim.keymap.set('n', 'gÉ', ':exe "silent! tablast"<CR>')
-- optionnel : {g"} pour aller au début de la ligne écran
vim.keymap.set('n', 'g"', 'g0')

-- <> en direct
-- ————————————
vim.keymap.set('', '«', '<')
vim.keymap.set('', '»', '>')

-- [] en direct
-- ————————————
vim.keymap.set('n', '(', '[', { remap = true })
vim.keymap.set('n', ')', ']', { remap = true })

-- Remaper la gestion des fenêtres
-- ———————————————————————————————
-- Déplacements du curseur
vim.keymap.set('n', '<C-w>t', '<C-w>j')
vim.keymap.set('n', '<C-w>s', '<C-w>k')
vim.keymap.set('n', '<C-w>c', '<C-w>h')
vim.keymap.set('n', '<C-w>r', '<C-w>l')
vim.keymap.set('n', '<C-w>l', '<C-w>p')
-- Split
vim.keymap.set('n', '<C-w>d', '<C-w>c')
vim.keymap.set('n', '<C-w>h', '<C-w>s')
-- Déplacement des fenêtres
vim.keymap.set('n', '<C-w>m', '<C-w>r')
vim.keymap.set('n', '<C-w>M', '<C-w>R')
vim.keymap.set('n', '<C-w>T', '<C-w>J')
vim.keymap.set('n', '<C-w>S', '<C-w>K')
vim.keymap.set('n', '<C-w>C', '<C-w>H')
vim.keymap.set('n', '<C-w>R', '<C-w>L')
-- Simplification du <C-w> en w pour plus de rapidité
vim.keymap.set('n', 'w', '<C-w>', { remap = true })

-- colorise les nbsp
-- ———————————————————————————————
vim.cmd.highlight({'NbSp', 'ctermbg=lightgray', 'guibg=lightgray'})
vim.cmd.call('matchadd("NbSp", "\\%xA0")')
