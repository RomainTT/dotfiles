vim.g.surround_no_mappings = 1
vim.keymap.set('n', '<leader>sd', '<Plug>Dsurround')
vim.keymap.set('n', '<leader>sc', '<Plug>Csurround')
vim.keymap.set('n', '<leader>sC', '<Plug>CSurround')
vim.keymap.set('n', '<leader>sy', '<Plug>Ysurround')
vim.keymap.set('n', '<leader>sY', '<Plug>YSurround')
vim.keymap.set('n', '<leader>sys', '<Plug>Yssurround')
vim.keymap.set('n', '<leader>sYs', '<Plug>YSsurround')
vim.keymap.set('x', 'S', '<Plug>VSurround')
vim.keymap.set('x', 'gS', '<Plug>VgSurround')
vim.keymap.set('i', '<C-S>', '<Plug>Isurround')
-- A shortcut to add surroundings to the current word
vim.keymap.set('n', '<leader>sw', '<leader>syiw')
vim.keymap.set('n', '<leader>sW', '<leader>syiW')
