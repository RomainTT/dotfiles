local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local feedkey = function(key, mode)
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
end

local cmp = require'cmp'
	cmp.setup({
	-- Menu Type (https://github.com/hrsh7th/nvim-cmp/wiki/Menu-Appearance#menu-type)
	view = {            
		entries = "custom" -- can be "custom", "wildmenu" or "native"
	},
	
	-- set sources
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
		{ name = 'nvim_lsp_signature_help' },
		{ name = 'vsnip' },
		{ name = 'emoji' },  -- from cmp-emoji plugin
		{ name = 'path' },  -- from cmp-path plugin
		{ name = 'buffer' },  -- from cmp-buffer plugin
		{ name = 'spell' },  -- from f3fora/cmp-spell plugin
	}),

	-- set snippet
	snippet = {
		expand = function(args)
			vim.fn["vsnip#anonymous"](args.body)
		end,
	},

	-- Configure custom mappings
	mapping = cmp.mapping.preset.insert({
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
			end
		end, {"i", "s"}),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			else
				fallback()
			end
		end, {"i", "s"}),
		["<C-n>"] = cmp.mapping(function(fallback)
			if vim.fn["vsnip#jumpable"](1) == 1 then
				feedkey("<Plug>(vsnip-jump-next)", "")
			else
				fallback()
			end
		end, {"i", "s", "n"}),
		["<C-p>"] = cmp.mapping(function(fallback)
			if vim.fn["vsnip#jumpable"](-1) == 1 then
				feedkey("<Plug>(vsnip-jump-prev)", "")
			else
				fallback()
			end
		end, {"i", "s", "n"}),
		['<C-d>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
		['<C-u>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
		['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
		['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
		['<C-e>'] = cmp.mapping({
			i = cmp.mapping.abort(),
			c = cmp.mapping.close(),
		}),
		["<CR>"] = cmp.mapping({
			i = function(fallback)
				if cmp.visible() and cmp.get_active_entry() then
					cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
				else
					fallback()
				end
			end,
			s = cmp.mapping.confirm({ select = true }),
			c = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false }),
		}),
		['<C-s>'] = cmp.mapping.complete({
			config = {
				sources = {
					{ name = 'vsnip' }
				}
			}
		})
	}),
})

-- Commandline setup
require'cmp'.setup.cmdline(':', {
	sources = {
		{ name = 'cmdline' },	-- from cmp-cmdline plugin
		{ name = 'path' },	-- from cmp-path plugin
	}
})

-- The following line fixes an unexpected effect of vim-cmp in command line mode
vim.api.nvim_set_keymap('c', '<tab>', '<C-z>', { silent = false })
