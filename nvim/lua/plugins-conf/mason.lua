-- For documentation, you can refer to the following links:
-- - https://github.com/neovim/nvim-lspconfig
-- - https://github.com/williamboman/mason.nvim

local mason = require('mason')
local mason_lspconfig = require('mason-lspconfig')
local lspconfig = require('lspconfig')

mason.setup()
mason_lspconfig.setup()

-- I found those lines here :
-- https://github.com/williamboman/mason.nvim/discussions/57
mason_lspconfig.setup_handlers {
		-- This is a default handler that will be called for each installed server
		-- (also for new servers that are installed during a session)
		function (server_name)
				lspconfig[server_name].setup {}
		end,
}
