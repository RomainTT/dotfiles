vim.g.airline_theme = 'understated'
vim.g['airline#extensions#branch#enabled'] = 1
vim.g.airline_powerline_fonts = 0
vim.g.airline_section_x = '%{ConflictedVersion()}'
vim.g.airline_section_z = '☰ %l/%L:%v'
