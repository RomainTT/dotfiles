-- A lua configuration file for formatter.nvim plugin.
-- https://github.com/mhartington/formatter.nvim
---------------------------------------------------------

-- Utilities for creating configurations
local util = require "formatter.util"

-- Provides the Format, FormatWrite, FormatLock, and FormatWriteLock commands
require("formatter").setup {
  -- Enable or disable logging
  logging = true,
  -- Set the log level
  log_level = vim.log.levels.WARN,
  -- All formatter configurations are opt-in
  filetype = {
    -- Formatter configurations for filetype "python" go here
    -- and will be executed in order
    python = {
      -- "formatter.filetypes.python" defines default configurations for the
      -- "python" filetype
	  -- Available formatters are here:
	  -- https://github.com/mhartington/formatter.nvim/blob/master/lua/formatter/filetypes/python.lua
      require("formatter.filetypes.python").black,
      require("formatter.filetypes.python").isort,
	},

	json = {
      require("formatter.filetypes.json").jq,
	},

	php = {
      require("formatter.filetypes.php").php_cs_fixer,
	},

	ruby = {
      require("formatter.filetypes.ruby").rubocop
	},

    -- Use the special "*" filetype for defining formatter configurations on
    -- any filetype
    ["*"] = {
      -- "formatter.filetypes.any" defines default configurations for any
      -- filetype
      require("formatter.filetypes.any").remove_trailing_whitespace
    }
  }
}

-- Keymaps
local map = vim.api.nvim_set_keymap
local options = { noremap = true }
map('n', '<leader>a', ':Format<CR>', options)
