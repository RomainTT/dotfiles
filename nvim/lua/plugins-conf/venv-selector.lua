local vs = require('venv-selector')

vs.setup {
   name = {"venv", ".venv"},
}
