vim.g.NERDTreeMapActivateNode = 'oo'
vim.g.NERDTreeMapPreview = 'oO'
vim.g.NERDTreeMapOpenInTab = 'ot'
vim.g.NERDTreeMapOpenInTabSilent = 'oT'
vim.g.NERDTreeMapOpenSplit = 'oh'
vim.g.NERDTreeMapPreviewSplit = 'oH'
vim.g.NERDTreeMapOpenVSplit = 'ov'
vim.g.NERDTreeMapPreviewVSplit = 'oV'
vim.g.NERDTreeMapOpenRecursively = 'oR'
vim.g.NERDTreeMapJumpNextSibling = 'r'
vim.g.NERDTreeMapJumpFirstChild = 'C'
vim.g.NERDTreeMapJumpPrevSibling= 'c'
vim.g.NERDTreeMapJumpParent = 'S'
vim.g.NERDTreeMapJumpLastChild = 'R'
vim.g.NERDTreeMapChangeRoot = 'gcr'
vim.g.NERDTreeMapRefresh = 'gr'
vim.g.NERDTreeMapRefreshRoot = 'gR'
vim.g.NERDTreeMapChdir = 'gcd'
vim.g.NERDTreeMapToggleHidden = 'h'
vim.g.NERDTreeMapCWD = 'gCD'
vim.g.NERDTreeWinSize = 22

-- Start NERDTree if nvim is launched from a git repo
-- and no file is opened at start
local code = os.execute('git rev-parse --is-inside-work-tree')
if code == 0 then
		vim.api.nvim_create_autocmd(
				{'VimEnter'},
				{ pattern = '*', command = 'NERDTree | Tagbar' }
		)
end
