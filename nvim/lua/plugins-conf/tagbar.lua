vim.g.tagbar_width=25
vim.g.tagbar_position='belowright'
vim.g.tagbar_map_hidenonpublic='h'
vim.g.tagbar_height = vim.api.nvim_win_get_height(0) / 2
vim.g.tagbar_map_togglesort='gs'
vim.g.tagbar_map_togglepause='gt'
