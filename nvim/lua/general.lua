-- Very general configuration of Neovim

-- Set theme
-- (must be done before anything else because it clears highlights)
vim.opt.background = 'light'
vim.opt.termguicolors = true
vim.g.gruvbox_contrast_light = 'hard'
vim.g.gruvbox_number_column = 'bg0'
vim.g.gruvbox_sign_column = 'bg0'
vim.g.gruvbox_color_column = 'bg0'
vim.g.gruvbox_vert_split = 'bg0'
vim.g.gruvbox_invert_tabline = 1
vim.cmd.colorscheme('gruvbox')

-- Enable syntax highlighting
vim.cmd.syntax('on')

-- Turn ON line numbering
vim.opt.nu = true

-- Do not let the curser get too close to the edge
vim.opt.scrolloff = 3

-- Set default tabstop to 4
vim.opt.tabstop = 4

-- Enable folding
vim.opt.foldmethod = 'indent'
vim.opt.foldlevel = 99

-- Make Vim automatically read the file again
-- if it has been modified externally
vim.opt.autoread = true

-- Highlight search matches
vim.opt.hlsearch = true

-- Change default color of matches
vim.cmd.highlight({'Search', 'cterm=NONE', 'ctermfg=black', 'ctermbg=yellow'})

-- For Git diff
vim.opt.diffopt:append('vertical')

-- Enable folding with the spacebar
vim.keymap.set('n', '<space>', 'za')
--
-- Press Return to temporarily get out of the highlighted search
vim.keymap.set('n', '<CR>', ':nohlsearch<CR><CR>')

-- Search and replace the current word with <Leader>h
vim.keymap.set('n', '<Leader>h', ':%s/\\<<C-r><C-w>\\>//g<Left><Left>')

-- Search the current selection with / in visual mode
vim.keymap.set('v', '/', 'y<ESC>/\\V<c-r>=escape(@",\'/\\:\')<CR><CR>')

-- Shortcut to save file
vim.keymap.set('n', '<Leader>w', ':w<CR>')

-- Spell check
vim.opt.spell = true
vim.opt.spelllang = { 'en_us' }

-- Turn off mouse
vim.opt.mouse = ""

-- Switch from Terminal mode to Normal mode with Esc
vim.keymap.set('t', '<esc>', [[<C-\><C-n>]])
