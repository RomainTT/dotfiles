" Use python from the dedicated venv
lua vim.g.python3_host_prog = '~/.virtualenvs/nvim/bin/python'

lua require('bepo')
lua require('plugins')
lua require('plugins-conf.mason')
lua require('plugins-conf.cmp')
lua require("plugins-conf.formatter")
lua require("plugins-conf.nerdtree")
lua require("plugins-conf.airline")
lua require("plugins-conf.anyfold")
lua require("plugins-conf.echodoc")
lua require("plugins-conf.indentline")
lua require("plugins-conf.neomake")
lua require("plugins-conf.signature")
lua require("plugins-conf.surround")
lua require("plugins-conf.tagbar")
lua require("plugins-conf.telescope")
lua require("plugins-conf.venv-selector")
lua require('navigation')
lua require('completion')
lua require('diagnostic')
lua require('general')

" Add custom window submode
source ~/.config/nvim/custom/window_submode.vim
